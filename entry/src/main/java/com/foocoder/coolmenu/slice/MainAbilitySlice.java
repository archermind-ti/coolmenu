package com.foocoder.coolmenu.slice;

import com.dxtt.coolmenu.CoolMenuFrameLayout;
import com.dxtt.coolmenu.FractionListPageAdapter;
import com.foocoder.coolmenu.MainAbility;
import com.foocoder.coolmenu.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainAbilitySlice extends AbilitySlice {

    private static final HiLogLabel hilog = new HiLogLabel(HiLog.DEBUG, 0x0000, "menu_log");
    CoolMenuFrameLayout coolMenuFrameLayout;
    List<Fraction> fractions = new ArrayList<>();
    List<String> titleList = null;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        coolMenuFrameLayout = (CoolMenuFrameLayout) findComponentById(ResourceTable.Id_rl_main);

        fractions.add(new Fraction1());
        fractions.add(new Fraction2());
        fractions.add(new Fraction3());
        fractions.add(new Fraction4());

        FractionListPageAdapter fractionList = new FractionListPageAdapter(
                ((MainAbility) getAbility()).getFractionManager(),
                ResourceTable.Id_rl_main) {
            @Override
            public int getCount() {
                return fractions.size();
            }

            @Override
            public Fraction getItem(int position) {
                return fractions.get(position);
            }
        };

        coolMenuFrameLayout.setAdapter(fractionList);
        String[] titles = {"CONTACT", "ABOUT", "TEAM", "PROJECTS"};
        titleList = Arrays.asList(titles);
        coolMenuFrameLayout.setTitles(titleList);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
