package com.dxtt.coolmenu;

import ohos.app.Context;

final class Utils {

    private Utils() {
    }

    public static int getScreenWidth(Context context) {
        return context.getResourceManager().getDeviceCapability().width
                * context.getResourceManager().getDeviceCapability().screenDensity
                / 160;
    }

    public static int getScreenHeight(Context context) {
        return context.getResourceManager().getDeviceCapability().height
                * context.getResourceManager().getDeviceCapability().screenDensity
                / 160;
    }
}
