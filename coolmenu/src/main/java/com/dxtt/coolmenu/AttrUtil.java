/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.dxtt.coolmenu;

import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;
import ohos.agp.utils.Color;

/**
 * 自定义属性工具类
 */
public class AttrUtil {

    AttrSet attrSet;

    public AttrUtil(AttrSet attrSet) {
        this.attrSet = attrSet;
    }

    /**
     * 获取自定义String
     *
     * @param key      键值
     * @param defValue 默认值
     * @return 取到的自定义属性，如没有，则返回默认值
     */
    public String getStringValue(String key, String defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getStringValue();
        } else {
            return defValue;
        }
    }

    /**
     * 获取自定义Interger
     *
     * @param key      键值
     * @param defValue 默认值
     * @return 取到的自定义属性，如没有，则返回默认值
     */
    public int getIntegerValue(String key, int defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getIntegerValue();
        } else {
            return defValue;
        }
    }

    /**
     * 获取自定义Boolean
     *
     * @param key      键值
     * @param defValue 默认值
     * @return 取到的自定义属性，如没有，则返回默认值
     */
    public boolean getBooleanValue(String key, boolean defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getBoolValue();
        } else {
            return defValue;
        }
    }

    /**
     * 获取自定义Float
     *
     * @param key      键值
     * @param defValue 默认值
     * @return 取到的自定义属性，如没有，则返回默认值
     */
    public float getFloatValue(String key, float defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getFloatValue();
        } else {
            return defValue;
        }
    }

    /**
     * 获取自定义Long
     *
     * @param key      键值
     * @param defValue 默认值
     * @return 取到的自定义属性，如没有，则返回默认值
     */
    public long getLongValue(String key, long defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getLongValue();
        } else {
            return defValue;
        }
    }

    /**
     * 获取自定义Element
     *
     * @param key      键值
     * @param defValue 默认值
     * @return 取到的自定义属性，如没有，则返回默认值
     */
    public Element getElement(String key, Element defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getElement();
        } else {
            return defValue;
        }
    }

    /**
     * 获取自定义Dimension
     *
     * @param key      键值
     * @param defValue 默认值
     * @return 取到的自定义属性，如没有，则返回默认值
     */
    public int getDimensionValue(String key, int defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getDimensionValue();
        } else {
            return defValue;
        }
    }

    /**
     * 获取自定义Color,默认值为Color类型
     *
     * @param key      键值
     * @param defValue 默认值
     * @return 取到的自定义属性，如没有，则返回默认值
     */
    public Color getColorValue(String key, Color defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getColorValue();
        } else {
            return defValue;
        }
    }

    /**
     * 获取自定义，默认值为int类型
     *
     * @param key      键值
     * @param defValue 默认值
     * @return 取到的自定义属性，如没有，则返回默认值
     */
    public int getColorValue(String key, int defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getColorValue().getValue();
        } else {
            return defValue;
        }
    }

}