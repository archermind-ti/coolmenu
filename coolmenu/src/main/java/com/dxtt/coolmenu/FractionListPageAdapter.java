/**
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.dxtt.coolmenu;

import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.ability.fraction.FractionManager;
import ohos.aafwk.ability.fraction.FractionScheduler;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.PageSliderProvider;
import ohos.utils.PlainArray;

import java.util.Optional;

public abstract class FractionListPageAdapter extends PageSliderProvider {
    private final FractionManager mManager;
    private FractionScheduler mCurTransaction = null;

    private PlainArray<Optional<Fraction>> mSavedState = new PlainArray<>();
    private PlainArray<Fraction> mFractions = new PlainArray<>();
    private Fraction mCurrentPrimaryItem = null;
    private int containerId;

    public FractionListPageAdapter(FractionManager fm, int containerId) {
        this.mManager = fm;
        this.containerId = containerId;
    }

    /**
     * Return the Fragment associated with a specified position.
     */
    public abstract Fraction getItem(int position);

    @Override
    public void startUpdate(ComponentContainer container) {
    }

    public Fraction getExitFraction(int position) {
        return mFractions.valueAt(position);
    }

    public Fraction getCurrentFragment() {
        return mCurrentPrimaryItem;
    }

    @Override
    public Object createPageInContainer(ComponentContainer container, int position) {
        Fraction f = mFractions.get(position, null);
        if (f != null) {
            return f;
        }

        if (mCurTransaction == null) {
            mCurTransaction = mManager.startFractionScheduler();
        }

        Fraction fragment = getItem(position);
        mFractions.put(position, fragment);
        mCurTransaction.add(containerId, fragment, "fraction");
        return fragment;
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer container, int position, Object object) {
        Fraction fraction = (Fraction) object;

        if (mCurTransaction == null) {
            mCurTransaction = mManager.startFractionScheduler();
        }
        mFractions.remove(position);
        mCurTransaction.remove(fraction);
    }

    @Override
    public void onUpdateFinished(ComponentContainer container) {
        if (mCurTransaction != null) {
            mCurTransaction.submit();
            mCurTransaction = null;
            mManager.startFractionScheduler();
        }
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object object) {
        return ((Fraction) object).getComponent() == component;
    }
}
