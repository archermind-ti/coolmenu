package com.dxtt.coolmenu;

import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;


class TranslateLayout extends StackLayout implements
        Component.ClickedListener,
        Component.TouchEventListener,
        ComponentContainer.ArrangeListener {

    private OnMenuClickListener mOnMenuClickListener;

    private Image mMenu;

    private Text mTitle;

    private float mTitleTrans;
    private Component view;

    public TranslateLayout(Context context) {
        super(context);

        init();
    }

    public TranslateLayout(Context context, AttrSet attrSet) {
        super(context, attrSet);

        init();
    }

    public TranslateLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);

        init();
    }

    private void init() {
        view = LayoutScatter.getInstance(this.getContext()).parse(ResourceTable.Layout_layout_tilte, null, false);
        addComponent(view);
        view.setTouchEventListener(new TouchEventListener() {
            @Override
            public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                return true;
            }
        });

        mTitleTrans = 40;
        mMenu = (Image) view.findComponentById(ResourceTable.Id_image_menu);
        mTitle = (Text) view.findComponentById(ResourceTable.Id_text_title);
        mMenu.setClickedListener(this);
        setMenuAlpha(0);

        setTouchEventListener(this);
        setArrangeListener(this);
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        return ((CoolMenuFrameLayout) getComponentParent()).isOpening();
    }

    @Override
    public boolean onArrange(int left, int top, int width, int height) {
        if (getChildIndex(view) != getChildCount() - 1) {
            moveChildToFront(view);
        }
        return false;
    }

    private float getX() {
        return getLeft() + getTranslationX();
    }

    private void setX(float x) {
        setTranslationX(x - getLeft());
    }

    private float getY() {
        return getTop() + getTranslationY();
    }

    private void setY(float y) {
        setTranslationY(y - getTop());
    }

    public float getXFraction() {
        int width = Utils.getScreenWidth(getContext());
        return (width == 0) ? 0 : getX() / (float) width;
    }

    public void setXFraction(float xFraction) {
        int width = Utils.getScreenWidth(getContext());
        setX((width > 0) ? (xFraction * width) : 0);
    }

    public float getYFraction() {
        int height = Utils.getScreenHeight(getContext());
        return (height == 0) ? 0 : getY() / (float) height;
    }

    public void setYFraction(float yFraction) {
        int height = Utils.getScreenHeight(getContext());
        setY((height > 0) ? (yFraction * height) : 0);
    }

    public void setOnMenuClickListener(OnMenuClickListener mOnMenuClickListener) {
        this.mOnMenuClickListener = mOnMenuClickListener;
    }

    public void setTitle(String title) {
        mTitle.setText(title);
    }

    public void setMenuIcon(Element drawable) {
        mMenu.setImageElement(drawable);
    }

    public void setMenuIcon(int resId) {
        mMenu.setPixelMap(resId);
    }

    public void setMenuTitleSize(int size) {
        mTitle.setTextSize(size, Text.TextSizeType.PX);//TypedValue.COMPLEX_UNIT_PX
    }

    public void setMenuTitleColor(Color color) {
        mTitle.setTextColor(color);
    }

    public void setMenuAlpha(float fraction) {
        mMenu.setAlpha(fraction);
        mMenu.setScaleX(fraction);
        mMenu.setScaleY(fraction);
        mTitle.setTranslationX((1 - fraction) * -mTitleTrans);
    }

    @Override
    public void onClick(Component v) {
        if (ResourceTable.Id_image_menu == v.getId()) {
            if (mOnMenuClickListener != null) {
                mOnMenuClickListener.onMenuClick();
            }
        }
    }

    interface OnMenuClickListener {
        void onMenuClick();
    }
}
