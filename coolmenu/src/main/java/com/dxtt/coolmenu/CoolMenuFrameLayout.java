package com.dxtt.coolmenu;

import ohos.aafwk.ability.fraction.Fraction;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.database.DataSetSubscriber;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static ohos.agp.components.ComponentContainer.LayoutConfig.MATCH_PARENT;

public final class CoolMenuFrameLayout extends StackLayout {
    private static final HiLogLabel TAG = new HiLogLabel(HiLog.DEBUG, 0x0000, "menu_log");
    private final int mInterpolator = Animator.CurveType.ACCELERATE_DECELERATE;
    private int num = 3;
    private Color mTitleColor;
    private int mTitleSize;
    private Element mMenuIcon;
    private int[] ids = {0, 1, 2, 3, 4};
    private AnimatorValue[] mOpenAnimators;

    private AnimatorValue[] mChosenAnimators;

    private AnimatorValue[] mMenuOpenAnimators;

    private AnimatorValue[] mOpenReverseAnimators;

    private AnimatorValue[] mChosenReverseAnimators;

    private AnimatorValue[] mMenuOpenReverseAnimators;


    private FractionListPageAdapter mAdapter;

    private MenuObserver mObserver;

    private final MenuChooser mMenuChooser = new MenuChooser();

    private final TranslateLayout.OnMenuClickListener menuListener = new MenuListener();

    private boolean opening = false;

    private List<Object> objects = new ArrayList<>();

    private int chosen;

    public CoolMenuFrameLayout(Context context) {
        this(context, null);
    }

    public CoolMenuFrameLayout(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public CoolMenuFrameLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        mContext = context;
        AttrUtil attrUtil = new AttrUtil(attrSet);
        num = attrUtil.getIntegerValue("num", 3);
        mTitleColor = attrUtil.getColorValue("titleColor", Color.BLACK);
        mTitleSize = attrUtil.getDimensionValue("titleSize", AttrHelper.convertDimensionToPix(context, "20vp", 20));

        try {
            mMenuIcon = attrUtil.getElement("titleIcon", new PixelMapElement(context.getResourceManager().getResource(ResourceTable.Media_menu)));
        } catch (IOException | NotExistException e) {
            e.printStackTrace();
        }
        init();
    }


    private void init() {
        chosen = num - 1;
        mOpenAnimators = new AnimatorValue[num];
        mChosenAnimators = new AnimatorValue[num];
        mMenuOpenAnimators = new AnimatorValue[num];
        mOpenReverseAnimators = new AnimatorValue[num];
        mChosenReverseAnimators = new AnimatorValue[num];
        mMenuOpenReverseAnimators = new AnimatorValue[num];
    }

    private void initAnim() {
        for (int i = 0; i < getChildCount(); i++) {
            TranslateLayout child = (TranslateLayout) getComponentAt(i);
            float transX = (float) ((i + 1) * 0.06);
            float transY = (float) ((i + 1) * 0.12);

            mOpenAnimators[i] = makeOpenMenuAnimatorSet(0, transX, 0, transY, child);
            mOpenReverseAnimators[i] = makeOpenMenuAnimatorSet(transX, 0, transY, 0, child);

            mChosenAnimators[i] = makeOpenMenuAnimatorSetOnlyX(transX, 1, child);
            mChosenReverseAnimators[i] = makeOpenMenuAnimatorSetOnlyX(1, transX, child);

            mMenuOpenAnimators[i] = makeOpenMenuAnimatorSetAlpha(1, 0, child);
            mMenuOpenReverseAnimators[i] = makeOpenMenuAnimatorSetAlpha(0, 1, child);
        }
    }


    public void setAdapter(FractionListPageAdapter adapter) {
        if (mAdapter != null) {
            mAdapter.removeDataSubscriber(mObserver);
            mAdapter.startUpdate(this);
            for (int i = 0; i < num; i++) {
                mAdapter.destroyPageFromContainer((ComponentContainer) getComponentAt(i + 1), i, objects.get(i));
            }
            mAdapter.onUpdateFinished(this);
        }

        this.mAdapter = adapter;
        if (mAdapter == null) {
            return;
        }

        if (mObserver == null) {
            mObserver = new MenuObserver();
        }
        mAdapter.addDataSubscriber(mObserver);

        int count = mAdapter.getCount();
        if (count != num) {
            throw new RuntimeException("number of view should equal 'num' that declared in xml");
        }

        for (int i = 0; i < count; i++) {
            ComponentContainer container = (ComponentContainer) getComponentAt(i + 1);
            Object object = mAdapter.createPageInContainer(container, i);
            objects.add(object);
        }
        mAdapter.onUpdateFinished(this);
        LayoutConfig layoutParams = new LayoutConfig(MATCH_PARENT, MATCH_PARENT);
        for (int i = 0; i < count; i++) {
            TranslateLayout frameLayout = new TranslateLayout(getContext());

            ComponentContainer componentContainer = (ComponentContainer) adapter.getItem(i).getComponent().getComponentParent();
            if (componentContainer != null) {
                componentContainer.removeComponent(adapter.getItem(i).getComponent());
            }

            frameLayout.addComponent(adapter.getItem(i).getComponent());
            frameLayout.setId(i + 1);
            frameLayout.setTag(i);
            frameLayout.setClickedListener(mMenuChooser);
            frameLayout.setOnMenuClickListener(menuListener);
            if (mMenuIcon != null) {
                frameLayout.setMenuIcon(mMenuIcon);
            }
            frameLayout.setMenuTitleSize(mTitleSize);
            frameLayout.setMenuTitleColor(mTitleColor);
            if (i == num - 1) frameLayout.setMenuAlpha(1);
            frameLayout.setLayoutConfig(layoutParams);
            addComponent(frameLayout);
        }

        mAdapter.onUpdateFinished(this);
        initAnim();
    }

    public void setTitles(List<String> titles) {
        checkAdapter();
        for (int i = 0; i < num; i++) {
            ((TranslateLayout) getComponentAt(i)).setTitle(titles.get(i));
        }
    }

    public void setTitleByIndex(String title, int index) {
        checkAdapter();
        if (index > num - 1) {
            throw new IndexOutOfBoundsException();
        }
        ((TranslateLayout) getComponentAt(index)).setTitle(title);
    }

    public void setMenuIcon(int resId) {
        checkAdapter();
        for (int i = 0; i < num; i++) {
            ((TranslateLayout) getComponentAt(i)).setMenuIcon(resId);
        }
    }

    public void toggle() {
        if (opening) {
            close();
        } else {
            open();
        }
    }

    private void open() {
        checkAdapter();
        opening = true;
        for (int i = 0; i < num; i++) {
            if (i == chosen) {
                mMenuOpenAnimators[i].start();
                mOpenAnimators[i].start();
            } else if (i > chosen) {
                mChosenReverseAnimators[i].start();
            } else {
                mOpenAnimators[i].start();
            }
        }
        chosen = num - 1;
    }

    private void close() {
        checkAdapter();
        opening = false;
        for (AnimatorValue mAnimator : mOpenReverseAnimators) {
            mAnimator.start();
        }
    }

    private void dataSetChanged() {
        if (opening) {
            close();
        }
        if (mAdapter != null) {
            mAdapter.startUpdate(this);
            mAdapter.onUpdateFinished(this);//.finishUpdate(this);
            for (int i = 0; i < num; i++) {
                TranslateLayout frameLayout = ((TranslateLayout) getComponentAt(i));
                Fraction fraction = (Fraction) objects.get(i);
                ComponentContainer componentContainer = (ComponentContainer) fraction.getComponent().getComponentParent();
                if (componentContainer != null) {
                    componentContainer.removeComponent(fraction.getComponent());
                }
                frameLayout.addComponent(fraction.getComponent());
            }
            mAdapter.onUpdateFinished(this);
        }
    }

    boolean isOpening() {
        return opening;
    }

    private void checkAdapter() {
        if (mAdapter == null) {
            throw new RuntimeException("Must Set Adapter firstly!!");
        }
    }

    private AnimatorValue makeOpenMenuAnimatorSet(
            float startTranX, float endTranX,
            float startTranY, float endTranY,
            TranslateLayout transitionLayout) {
        AnimatorValue animatorValue = new AnimatorValue();
        float dx = endTranX - startTranX;
        float dy = endTranY - startTranY;

        animatorValue.setValueUpdateListener((animatorValue1, v) -> {

            transitionLayout.setXFraction(startTranX + dx * v);
            transitionLayout.setYFraction(startTranY + dy * v);
        });

        animatorValue.setDuration(300);
        animatorValue.setCurveType(mInterpolator);
        return animatorValue;
    }

    private AnimatorValue makeOpenMenuAnimatorSetOnlyX(float startX, float endTranX, TranslateLayout transitionLayout) {

        AnimatorValue animatorValue = new AnimatorValue();
        float dx = endTranX - startX;
        animatorValue.setValueUpdateListener((animatorValue1, v) -> {
            transitionLayout.setXFraction(startX + dx * v);
        });

        animatorValue.setDuration(300);
        animatorValue.setCurveType(mInterpolator);
        return animatorValue;
    }

    private AnimatorValue makeOpenMenuAnimatorSetAlpha(float startAlpha, float endAlpha, TranslateLayout transitionLayout) {
        AnimatorValue animatorValue = new AnimatorValue();
        float alpha = endAlpha - startAlpha;

        animatorValue.setValueUpdateListener((animatorValue1, v) ->
                transitionLayout.setMenuAlpha(alpha > 0 ? alpha * v : alpha * (v - 1))
        );
        animatorValue.setDuration(300);
        animatorValue.setCurveType(mInterpolator);
        return animatorValue;
    }

    private class MenuObserver extends DataSetSubscriber { //DataSetObserver
        @Override
        public void onChanged() {
            dataSetChanged();
        }

        @Override
        public void onInvalidated() {
            dataSetChanged();
        }
    }

    private class MenuChooser implements ClickedListener {

        @Override
        public void onClick(Component v) {
            if (opening) {
                chosen = (int) v.getTag();
                for (int i = 0; i < num; i++) {
                    if (i <= chosen) {
                        mOpenReverseAnimators[i].start();
                    } else {
                        mChosenAnimators[i].start();
                    }
                }
                mMenuOpenReverseAnimators[chosen].start();
            }
            opening = false;
        }
    }

    private class MenuListener implements TranslateLayout.OnMenuClickListener {

        @Override
        public void onMenuClick() {
            toggle();
        }
    }
}
