# coolMenu

coolMenu 通过菜单方式切换Fraction

## 演示效果
![](img/coolmenu1.gif)




## 集成

With gradle:

```groovy
dependencies {
   compile 'com.gitee.archermind-ti:coolMenu:1.0.0'
}
```

## 使用方式

#### 布局文件

```xml
<?xml version="1.0" encoding="utf-8"?>
<DirectionalLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    xmlns:app="http://schemas.huawei.com/apk/res/ohos"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:alignment="center"
    ohos:background_element="black"
    ohos:orientation="vertical">

    <com.dxtt.coolmenu.CoolMenuFrameLayout
        ohos:id="$+id:rl_main"
        ohos:height="match_parent"
        ohos:width="match_parent"
        ohos:layout_alignment="center"
        app:num="4"
        />
</DirectionalLayout>
```

#### 代码设置

```java
      coolMenuFrameLayout = (CoolMenuFrameLayout) findComponentById(ResourceTable.Id_rl_main);

        fractions.add(new Fraction1());
        fractions.add(new Fraction2());
        fractions.add(new Fraction3());
        fractions.add(new Fraction4());

        FractionListPageAdapter fractionList = new FractionListPageAdapter(
                ((MainAbility) getAbility()).getFractionManager(),
                ResourceTable.Id_rl_main) {
            @Override
            public int getCount() {
                return fractions.size();
            }

            @Override
            public Fraction getItem(int position) {
                return fractions.get(position);
            }
        };

        coolMenuFrameLayout.setAdapter(fractionList);
        String[] titles = {"CONTACT", "ABOUT", "TEAM", "PROJECTS"};
        titleList = Arrays.asList(titles);
        coolMenuFrameLayout.setTitles(titleList);
```

## 变动说明

新增 ```FactionListPageAdapter```提供方便向控件提供Fraction信息。

## License

`coolMenu` is available under the MIT license

